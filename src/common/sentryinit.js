const Sentry = require("@sentry/electron")
Sentry.init({
  dsn: "https://b9d138140f67486f93a3c96eb9db4ff7@sentry.io/1358662",
  integrations: require("is-electron-renderer")
    ? integrations => {
        return integrations.filter(i => i.name !== "GlobalHandlers")
      }
    : undefined
})
