const httpProxy = require("http-proxy")
const http = require("http")
const url = require("url")
const qs = require("querystring")
const xml2js = require("xml2js")
const zlib = require("zlib")
const { Buffer } = require("buffer")
const EventEmitter = require("events")
const events = require("./events")
const https = require("https")
const logger = require("./logger").default("GameProxy")
const { asyncErrorBox } = require("../../common/misc")

class GameProxy extends EventEmitter {
  constructor(server) {
    super()
    this._host = new URL(server).host
    const agent = server.startsWith("https")
      ? new https.Agent({ keepAlive: true })
      : new http.Agent({ keepAlive: true })
    this._proxy = httpProxy.createProxyServer({
      target: server,
      changeOrigin: true,
      agent
    })
    this._proxy.on("error", this._handleError.bind(this))
    this._proxy.on("proxyReq", this._handleReq.bind(this))
    this._proxy.on("proxyRes", this._handle.bind(this))
    this._httpServer = http.createServer((req, res) => {
      this._proxy.web(req, res)
    })
    this._eventId = null
    this._personas = {}
  }

  listen(port) {
    this._httpServer.listen(port)
  }

  listenRandomPort() {
    return new Promise(resolve => {
      this._httpServer.once("listening", () => {
        logger.log(`Proxy listening on port ${this._httpServer.address().port}`)
        resolve(this._httpServer.address().port)
      })
      this.listen(0)
    })
  }

  close() {
    this._proxy.close()
  }

  _handleError(err, req, res) {
    logger.log("Proxy error: " + err.code)
    if (err.code == "ECONNRESET") {
      logger.log("Retrying")
      setTimeout(() => this._proxy.web(req, res), 1000)
    } else {
      res.writeHead(500)
      res.end("Proxy error: " + err.code)
      asyncErrorBox("Error", `Lost connection to server`)
      this._proxy.close()
    }
  }

  _handleReq(proxyReq, req, res, options) {
    // proxyReq.setHeader("Connection", "close")
  }

  _handle(proxyRes, req, finalRes) {
    proxyRes.headers.connection = "close"
    logger.log(`[${req.method}] ${req.url} (resp ${proxyRes.statusCode})`)
    if (proxyRes.statusCode < 200 || proxyRes.statusCode > 299) {
      return
    }
    let bufs = []
    let stream
    if (proxyRes.headers["content-encoding"] === "gzip") {
      stream = new zlib.Gunzip()
      proxyRes.pipe(stream)
    } else {
      stream = proxyRes
    }
    stream.on("data", data => {
      bufs.push(data)
    })
    stream.on("end", () => {
      const body = Buffer.concat(bufs).toString()
      const u = url.parse(req.url)
      const q = qs.parse(u.query)
      if (u.pathname.endsWith("/Engine.svc/User/GetPermanentSession")) {
        xml2js.parseString(body, (err, res) => {
          if (!res.UserInfo.personas[0]) return
          res.UserInfo.personas[0].ProfileData.forEach(p => {
            this._personas[p.PersonaId[0]] = {
              name: p.Name[0],
              icon: parseInt(p.IconIndex[0])
            }
          })
        })
      } else if (u.pathname.endsWith("/Engine.svc/User/SecureLoginPersona")) {
        const persona = this._personas[q.personaId]
        this.emit("logged_in", persona)
      } else if (
        u.pathname.endsWith("/Engine.svc/DriverPersona/UpdatePersonaPresence")
      ) {
        if (q.presence === "1") {
          this.emit("entered_freeroam")
        } else if (q.presence === "2") {
          this.emit("entered_safehouse")
        }
      } else if (u.pathname.endsWith("/Engine.svc/matchmaking/acceptinvite")) {
        xml2js.parseString(body, (err, res) => {
          const id = res.LobbyInfo.EventId[0]
          this._eventId = id
          this.emit("entered_lobby", {
            event: {
              id,
              name: events[id] || "<Unknown Event>"
            },
            players: res.LobbyInfo.Entrants ? res.LobbyInfo.Entrants.length : 0
          })
        })
        // } else if (u.pathname.includes("/Engine.svc/matchmaking/launchevent/")) {
        //   const splits = u.pathname.split("/")
        //   const id = splits[splits.length - 1]
        //   this.emit("entered_event", {
        //     id,
        //     name: events[id] || "<Unknown Event>"
        //   })
      } else if (u.pathname.includes("/Engine.svc/event/launched")) {
        this.emit("entered_event", {
          id: this._eventId,
          name: events[this._eventId] || "<Unknown Event>",
          session: parseInt(q.eventSessionId)
        })
      } else if (
        u.pathname.endsWith("/Engine.svc/DriverPersona/CreatePersona")
      ) {
        xml2js.parseString(body, { explicitArray: false }, (err, res) => {
          const p = res.ProfileData
          this._personas[p.PersonaId] = {
            name: p.Name,
            icon: parseInt(p.IconIndex)
          }
        })
      }
    })
  }
}

export { GameProxy }
