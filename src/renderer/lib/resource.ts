const path = require("path")
const fs = require("fs").promises

function getResourcePath(p: string) {
  if (path.isAbsolute(p)) throw new Error("Absolute path passed to getResource")
  if (require("electron").remote.require("process").defaultApp) {
    return path.resolve(process.cwd(), "resources", p)
  }
  return path.join((process as any).resourcesPath, "resources", p)
}

async function copyResource(src: string, dst: string) {
  await fs.copyFile(getResourcePath(src), dst)
}

export { getResourcePath, copyResource }
