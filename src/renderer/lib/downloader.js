const path = require("path")
const fs = require("fs-extra")
const xml2js = require("xml2js")
const got = require("got")
const EventEmitter = require("events")
const lzma = require("lzma-native")
const crypto = require("crypto")
const logger = require("./logger").default("Downloader")

const DownloadTypes = Object.freeze({
  base: "",
  tracks: "Tracks/",
  tracksHigh: "TracksHigh/",
  speech: "en/"
})

const IndexHashes = Object.freeze({
  [DownloadTypes.base]: "wy5ZU8EDqIb1vkCfNLxSN6BIKSHaQY9JWkktmpxglnQ=",
  [DownloadTypes.tracks]: "w4QNXEsHrPsmbDZKMHrjhpvE2cuAV5+UV8iZK610JPo=",
  [DownloadTypes.tracksHigh]: "KO71eva0TdwZuNhUXCtH9Wk7ZjmkrId0R3MRxQ65C3M=",
  [DownloadTypes.speech]: "NnMJ9v/ZXb8Q4HuILp7QvE8Fl1Rw86upG/EgBLCpAlY="
})

const SECTION_LENGTH = 1048576

class Downloader extends EventEmitter {
  constructor(options) {
    super()
    this._options = options
    this._indexes = {}
    this._currentProgress = null
    this._got = got.extend({
      baseUrl: options.cdnUrl
    })
  }

  async chooseCDN(list) {
    logger.log("Choosing best CDN")
    const progressUpdate = this._progressTask("Choosing CDN", list.length)
    const cdnSpeeds = []
    for (const cdn of list) {
      try {
        const start = Date.now()
        const length = (await got("index.xml", {
          baseUrl: cdn,
          timeout: 2500,
          encoding: null
        })).body.length
        cdnSpeeds.push([cdn, length / 1048576 / ((Date.now() - start) / 1000)])
      } catch (e) {}
      progressUpdate()
    }
    cdnSpeeds.sort((a, b) => b[1] - a[1])
    for (const cdn of cdnSpeeds) {
      logger.log(`${cdn[0]} - ${cdn[1].toFixed(2)} MB/s`)
    }
    this._got = got.extend({
      baseUrl: cdnSpeeds[0][0]
    })
    return cdnSpeeds[0][0]
  }

  async loadIndex() {
    const progressUpdate = this._progressTask(
      "Loading index",
      this._options.downloadTypes.length
    )
    for (const type of this._options.downloadTypes) {
      await this._loadSingleIndex(type)
      progressUpdate()
    }
  }

  async _loadSingleIndex(type) {
    const res = await this._got(`${type}index.xml`)
    const hash = crypto.createHash("sha256")
    hash.update(res.body)
    if (hash.digest("base64") != IndexHashes[type]) {
      throw new Error("Index validation failed for " + type)
    }
    return new Promise((resolve, reject) => {
      xml2js.parseString(res.body, { explicitArray: false }, (err, res) => {
        if (err) return reject(err)
        this._indexes[type] = {
          files: res.index.fileinfo.map(info => {
            const normPath = stringRemoveLeft(
              stringRemoveLeft(info.path, "CDShift"),
              "/"
            )
            return {
              path: path.join(normPath, info.file),
              hash: info.hash,
              offset: Number(info.offset),
              section: Number(info.section),
              length: info.compressed
                ? Number(info.compressed)
                : Number(info.length),
              compressed: !!info.compressed
            }
          }),
          sectionCount: Math.ceil(res.index.header.compressed / SECTION_LENGTH)
        }
        resolve()
      })
    })
  }

  _progressTask(task, total, eta = false) {
    this.emit("progress", { task, progress: 0, eta: null })
    let current = 0
    const startTime = Date.now()
    return (amount = 1) => {
      current += amount
      const progress = {
        task,
        progress: current / total,
        eta: null
      }
      if (eta) {
        const elapsed = Date.now() - startTime
        progress.eta = elapsed * (total / current) - elapsed
      }
      this.emit("progress", progress)
    }
  }

  async download() {
    const totalFileCount = Object.values(this._indexes).reduce(
      (acc, val) => acc + val.files.length,
      0
    )
    let progressUpdate = this._progressTask(
      "Discovering existing files",
      totalFileCount
    )
    const sectionList = {}
    for (const indexName in this._indexes) {
      const index = this._indexes[indexName]
      sectionList[indexName] = {
        sections: new Set(),
        files: new Set()
      }
      for (const file of index.files) {
        if (!fs.existsSync(path.join(this._options.directory, file.path))) {
          const sectionCount = Math.ceil(
            (file.offset + file.length) / SECTION_LENGTH
          )
          for (let i = 0; i < sectionCount; i++) {
            sectionList[indexName].sections.add(file.section + i)
          }
          sectionList[indexName].files.add(index.files.indexOf(file))
        }
        progressUpdate()
      }
    }
    const sectionCount = Object.values(sectionList).reduce(
      (acc, val) => acc + val.sections.size,
      0
    )
    console.log(`Downloading ${sectionCount} sections`)
    progressUpdate = this._progressTask("Downloading files", sectionCount, true)
    for (const indexName in this._indexes) {
      const index = this._indexes[indexName]
      const fileIndexIter = sectionList[indexName].files.values()
      let currentFile,
        toRead = 0,
        outputStream,
        fileClosed,
        currentFileIndex = fileIndexIter.next().value
      for (let i = 1; i <= index.sectionCount; i++) {
        if (toRead === 0 && !sectionList[indexName].sections.has(i)) {
          continue
        }
        // if (dlSpeedCalc.start && Date.now() - dlSpeedCalc.start > 2000) {
        //   // const FACTOR = 0.8
        //   // this._currentProgress.speed =
        //   //   FACTOR * this._currentProgress.speed +
        //   //   (1 - FACTOR) * (1 / ((Date.now() - speedCalcStart) / 1000))
        //   this._currentProgress.speed =
        //     dlSpeedCalc.accum /
        //     1048576 /
        //     ((Date.now() - dlSpeedCalc.start) / 1000)
        //   // const mbToDl =
        //   //   (this._currentProgress.totalBytes -
        //   //     this._currentProgress.downloadedBytes) /
        //   //   1048576
        //   // this._currentProgress.eta =
        //   //   (mbToDl / this._currentProgress.speed) * 1000
        //   dlSpeedCalc.start = Date.now()
        //   dlSpeedCalc.accum = 0
        // }

        const data = (await this._got(`${indexName}section${i}.dat`, {
          encoding: null
        })).body
        // Multi-section file
        if (toRead > 0) {
          const wBuf = data.slice(0, toRead)
          // console.log(`S${i} - Extending last file L${wBuf.length}`)
          outputStream.write(wBuf)
          toRead -= wBuf.length
          if (toRead > 0) {
            progressUpdate()
            continue
          }
          outputStream.end()
          await fileClosed
          const finalPath = path.join(this._options.directory, currentFile.path)
          fs.renameSync(`${finalPath}.dl`, finalPath)
          currentFileIndex = fileIndexIter.next().value
          if (!index.files[currentFileIndex]) {
            progressUpdate()
            continue
          }
        }

        while (true) {
          if (!sectionList[indexName].files.has(currentFileIndex)) {
            currentFileIndex = fileIndexIter.next().value
            if (
              !index.files[currentFileIndex] ||
              index.files[currentFileIndex].section != i
            ) {
              progressUpdate()
              break // Next section
            }
            continue
          }
          currentFile = index.files[currentFileIndex]
          if (currentFile.section != i) {
            throw new Error("Downloader internal state corrupted")
          }

          const finalPath = path.join(this._options.directory, currentFile.path)

          // if (await fs.exists(finalPath)) {
          //   this._currentProgress.downloadedBytes += currentFile.length
          //   this.emit("progress", this._currentProgress)
          //   currentFileIndex += 1
          //   if (
          //     !index.files[currentFileIndex] ||
          //     index.files[currentFileIndex].section != i
          //   ) {
          //     break
          //   }
          //   continue
          // }
          await fs.ensureDir(path.dirname(finalPath))
          const tmpPath = `${finalPath}.dl`
          const file = fs.createWriteStream(tmpPath)
          fileClosed = new Promise(resolve => file.on("close", resolve))

          if (currentFile.compressed) {
            outputStream = lzma.createDecompressor({ threads: 0 })
            outputStream.pipe(file)
          } else {
            outputStream = file
          }

          const offset = currentFile.offset
          toRead = currentFile.length

          const wBuf = data.slice(offset, offset + toRead)
          // console.log(
          //   `S${i} - New file ${currentFile.path} O${offset} L${wBuf.length}`
          // )
          outputStream.write(wBuf)
          toRead -= wBuf.length
          if (toRead > 0) {
            progressUpdate()
            break
          }
          outputStream.end()
          await fileClosed
          fs.renameSync(tmpPath, finalPath)
          currentFileIndex = fileIndexIter.next().value
          if (
            !index.files[currentFileIndex] ||
            index.files[currentFileIndex].section != i
          ) {
            progressUpdate()
            break
          }
        }

        // const file = this._files[i]
        // this._currentProgress.file = file.getPath()
        // this.emit("progress", this._currentProgress)
        // await file.download(this._options)
        // this._currentProgress.downloadedBytes += file.getDownloadLength()
        // this.emit("progress", this._currentProgress)
      }
    }
    if (sectionCount === 0 || true) {
      return
    }
    progressUpdate = this._progressTask("Verifying files", totalFileCount)
    for (const indexName in this._indexes) {
      const index = this._indexes[indexName]
      for (const file of index.files) {
        const finalPath = path.join(this._options.directory, file.path)
        const hash = crypto.createHash("md5")
        hash.setEncoding("base64")
        fs.createReadStream(finalPath).pipe(hash)
        const digest = await new Promise(resolve => {
          hash.once("finish", () => {
            resolve(hash.read())
          })
        })
        if (digest != file.hash) {
          console.log(`Hash mismatch: ${file.path}`)
        }
        progressUpdate()
      }
    }
  }
}

function stringRemoveLeft(s, r) {
  if (s.indexOf(r) === 0) {
    return s.slice(r.length)
  }
  return s
}

module.exports = {
  Downloader,
  DownloadTypes
}

if (process.env.NODE_ENV !== "production" && require.main === module) {
  const prettyMs = require("pretty-ms")
  async function main() {
    return chooseCDN([
      "https://download.soapboxraceworld.ml",
      "http://145.239.5.103/cdn/gamefiles/1614b",
      "http://mirror.nightriderz.world",
      "http://180.97.28.12:8765/ea_nfsw_section"
    ])
    const dl = new Downloader({
      downloadTypes: [DownloadTypes.base, DownloadTypes.speech],
      cdnUrl: "https://download.soapboxraceworld.ml",
      directory: "./game_files/"
    })
    dl.on("progress", p => {
      let log = `${p.task} - ${(p.progress * 100).toFixed(1)}`
      if (p.eta) {
        log += ` ETA: ${prettyMs(p.eta)})`
      }
      console.log(log)
    })
    await dl.loadIndex()
    await dl.download()
  }
  main()
}
