const Sentry = require("@sentry/electron")
import { asyncErrorBox } from "../../common/misc"

let errorBoxActive = false

async function unhandledError(error: any) {
  if (errorBoxActive) {
    return
  }
  let eventID, errorMsg
  if (error instanceof Error) {
    if (process.env.NODE_ENV === "production") {
      eventID = Sentry.captureException(error)
      errorMsg = `${error.name}: ${error.message}`
    } else {
      errorMsg = error.stack
    }
  } else {
    if (process.env.NODE_ENV === "production")
      eventID = Sentry.captureMessage(error.toString(), "error")
    errorMsg = error.toString()
  }
  errorBoxActive = true
  if (process.env.NODE_ENV === "production") {
    await asyncErrorBox("Unhandled error", `${errorMsg}\nEvent ID: ${eventID}`)
    await Sentry.getCurrentHub()
      .getClient()
      .close()
    window.close()
  } else {
    await asyncErrorBox("Unhandled error", errorMsg)
  }
  errorBoxActive = false
}

function registerHandlers() {
  window.addEventListener("error", e => unhandledError(e.error))
  window.addEventListener("unhandledrejection", (e: PromiseRejectionEvent) =>
    unhandledError(e.reason)
  )
}

export { unhandledError, registerHandlers }
