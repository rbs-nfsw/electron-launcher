import { getHWIDClient } from "./http"
import { Response, HTTPError } from "got"
const xml2js = require("xml2js")
const crypto = require("crypto")

class AuthError extends Error {}
class XMLParseError extends Error {}

interface AuthToken {
  email: string
  token: string
  userId: string
  warning?: string
}

function pwdHash(password: string) {
  const hash = crypto.createHash("sha1")
  hash.update(password)
  return hash.digest("hex")
}

async function responseXML(response: Response<string>): Promise<any> {
  return new Promise((resolve, reject) => {
    xml2js.parseString(response.body, { explicitArray: false }, (err, res) => {
      if (err) reject(new XMLParseError())
      else resolve(res)
    })
  })
}

async function registerLegacy(server: string, creds): Promise<AuthToken> {
  const hashedCreds = Object.assign({}, creds)
  hashedCreds.password = pwdHash(creds.password)
  const params = new URLSearchParams(hashedCreds)
  try {
    const res = await getHWIDClient(server)(`User/createUser?${params}`)
    const resXML = await responseXML(res)
    return {
      email: creds.email,
      token: resXML.LoginStatusVO.LoginToken,
      userId: resXML.LoginStatusVO.UserId,
      warning: resXML.LoginStatusVO.Warning
    }
  } catch (e) {
    if (e instanceof HTTPError) {
      const resXML = await responseXML(e.response)
      throw new AuthError(resXML.LoginStatusVO.Description)
    } else {
      throw e
    }
  }
}

async function loginLegacy(server: string, creds): Promise<AuthToken> {
  const hashedCreds = Object.assign({}, creds)
  hashedCreds.password = pwdHash(creds.password)
  const params = new URLSearchParams(hashedCreds)
  try {
    const res = await getHWIDClient(server)(`User/authenticateUser?${params}`)
    const resXML = await responseXML(res)
    return {
      email: creds.email,
      token: resXML.LoginStatusVO.LoginToken,
      userId: resXML.LoginStatusVO.UserId,
      warning: resXML.LoginStatusVO.Warning
    }
  } catch (e) {
    if (e instanceof HTTPError) {
      const resXML = await responseXML(e.response)
      if (resXML.LoginStatusVO.Ban) {
        let msg = `You have been banned with reason ${
          resXML.LoginStatusVO.Ban.Reason
        }`
        if (resXML.LoginStatusVO.Ban.Expires) {
          msg += `\nBan expires at ${resXML.LoginStatusVO.Ban.Expires}`
        }
        throw new AuthError(msg)
      }
      throw new AuthError(resXML.LoginStatusVO.Description)
    } else {
      throw e
    }
  }
}

async function registerModern(server: string, creds): Promise<string> {
  try {
    const res = await getHWIDClient(server)("User/modernRegister", {
      body: creds,
      json: true,
      method: "POST"
    })
    return res.body.message
  } catch (e) {
    if (e instanceof HTTPError && e.response.body.error) {
      throw new AuthError(e.response.body.error)
    } else {
      throw e
    }
  }
}

async function loginModern(server: string, creds): Promise<AuthToken> {
  try {
    const res = await getHWIDClient(server)("User/modernAuth", {
      body: {
        ...creds,
        upgrade: true
      },
      json: true,
      method: "POST"
    })
    return {
      email: creds.email,
      token: res.body.token,
      userId: res.body.userId,
      warning: res.body.warning
    }
  } catch (e) {
    if (e instanceof HTTPError && e.response.body.error) {
      throw new AuthError(e.response.body.error)
    } else {
      throw e
    }
  }
}

export {
  AuthError,
  XMLParseError,
  AuthToken,
  registerLegacy,
  registerModern,
  loginLegacy,
  loginModern,
  pwdHash as hashSHA1
}
