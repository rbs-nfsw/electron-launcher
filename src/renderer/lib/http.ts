import got = require("got")

function getHWIDClient(server: string): got.GotInstance<got.GotJSONFn> {
  return got.extend({
    baseUrl: server,
    timeout: 10000,
    headers: {
      "User-Agent": `electron-launcher/${VERSION}`
    }
  })
}

export { getHWIDClient }
