const ffi = require("ffi-napi")
const ref = require("ref-napi")
const Struct = require("ref-struct-di")(ref)

const ReplyStruct = Struct({
  Addr: "ulong",
  Status: "ulong",
  RoundTripTime: "ulong",
  DataSize: "ushort"
})

const voidPtr = ref.refType(ref.types.void)

const PingFFI = ffi.Library("Iphlpapi", {
  IcmpCreateFile: [voidPtr, []],
  IcmpCloseHandle: ["bool", [voidPtr]],
  IcmpSendEcho: [
    "uint",
    [voidPtr, "long", voidPtr, "uint16", voidPtr, voidPtr, "ulong", "ulong"]
  ]
})

class WinPing {
  constructor() {
    this._handle = PingFFI.IcmpCreateFile()
  }

  close() {
    PingFFI.IcmpCloseHandle(this._handle)
    this._handle = null
  }

  ping(ip) {
    if (!this._handle) {
      throw new Error("Ping instance closed")
    }

    const spl = ip.split(".")
    const aip = (+spl[3] << 24) | (+spl[2] << 16) | (+spl[1] << 8) | +spl[0]
    const data = Buffer.alloc(32)
    const reply = Buffer.alloc(40 + 32 + 8)
    return new Promise((resolve, reject) => {
      PingFFI.IcmpSendEcho.async(
        this._handle,
        aip,
        data,
        data.length,
        ref.NULL,
        reply,
        reply.length,
        1500,
        (err, ret) => {
          if (err) return reject(err)
          const replyStruct = new ReplyStruct(reply)
          if (ret === 0)
            return reject(
              new Error("Ping failed: StatusCode " + replyStruct.Status)
            )
          resolve(replyStruct.RoundTripTime)
        }
      )
    })
  }
}

module.exports["Ping"] = WinPing
