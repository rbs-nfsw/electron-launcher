const DiscordRPC = require("discord-rpc")
const logger = require("./logger").default("DiscordPresence")

function discordTime() {
  return Math.floor(Date.now() / 1000)
}

class DiscordPresence {
  constructor(clientId) {
    this._connected = false
    this._lastPresence = null
    this._server = null
    this._clientId = clientId
    this._presenceInLauncher()
    this._tryLogin()
  }

  _tryLogin() {
    this._rpc = new DiscordRPC.Client({ transport: "ipc" })
    this._rpc.login({ clientId: this._clientId }).then(
      () => {
        logger.log("Successfully connected to Discord RPC")
        this._connected = true
        if (this._lastPresence) {
          this._rpc.setActivity(this._lastPresence)
        }
      },
      err => {
        setTimeout(this._tryLogin.bind(this), 15000)
      }
    )
  }

  setServerName(server) {
    this._server = server
  }

  _updatePresence(presence) {
    if (this._connected) {
      this._rpc.setActivity(presence)
    } else {
      this._lastPresence = presence
    }
  }

  _presenceInLauncher() {
    this._updatePresence({
      details: "Electron Launcher",
      state:
        process.env.NODE_ENV === "production"
          ? "v" + VERSION
          : "Development Version"
    })
  }

  gameStart(proxy) {
    this._proxy = proxy
    this._proxy.on("logged_in", this._handleLoggedIn.bind(this))
    this._proxy.on("entered_freeroam", this._handleEnteredFreeroam.bind(this))
    this._proxy.on("entered_safehouse", this._handleEnteredSafehouse.bind(this))
    this._proxy.on("entered_lobby", this._handleEnteredLobby.bind(this))
    this._proxy.on("entered_event", this._handleEnteredEvent.bind(this))
    this._updatePresence({
      details: "Game starting...",
      state: this._server
    })
  }

  gameEnd() {
    this._proxy.removeListener("logged_in", this._handleLoggedIn.bind(this))
    this._proxy.removeListener(
      "entered_freeroam",
      this._handleEnteredFreeroam.bind(this)
    )
    this._proxy.removeListener(
      "entered_safehouse",
      this._handleEnteredSafehouse.bind(this)
    )
    this._proxy.removeListener(
      "entered_lobby",
      this._handleEnteredLobby.bind(this)
    )
    this._proxy.removeListener(
      "entered_event",
      this._handleEnteredEvent.bind(this)
    )
    this._proxy = null
    this._presenceInLauncher()
  }

  _handleLoggedIn(persona) {
    this._persona = persona
  }

  _handleEnteredFreeroam() {
    this._updatePresence({
      details: "Roaming around",
      state: this._server,
      largeImageKey: `avatar_${this._persona.icon}`,
      largeImageText: this._persona.name,
      startTimestamp: discordTime()
    })
  }

  _handleEnteredSafehouse() {
    this._updatePresence({
      details: "In Safehouse",
      state: this._server,
      largeImageKey: `avatar_${this._persona.icon}`,
      largeImageText: this._persona.name
    })
  }

  _handleEnteredLobby(evt) {
    this._updatePresence({
      details: evt.event.name,
      state: "In Lobby",
      largeImageKey: `avatar_${this._persona.icon}`,
      largeImageText: this._persona.name,
      partySize: evt.players,
      partyMax: 8
    })
  }

  _handleEnteredEvent(evt) {
    this._updatePresence({
      details: evt.name,
      state: this._server,
      largeImageKey: `avatar_${this._persona.icon}`,
      largeImageText: this._persona.name,
      startTimestamp: discordTime()
    })
  }
}

export { DiscordPresence }
